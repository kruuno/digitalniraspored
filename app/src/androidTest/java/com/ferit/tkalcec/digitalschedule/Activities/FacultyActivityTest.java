package com.ferit.tkalcec.digitalschedule.Activities;

import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import android.view.View;

import com.ferit.tkalcec.digitalschedule.R;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static junit.framework.TestCase.assertNotNull;


@RunWith(AndroidJUnit4.class)
@LargeTest
public class FacultyActivityTest {
    @Rule
    public ActivityTestRule<FacultyActivity> facultyActivityActivityTestRule = new ActivityTestRule<FacultyActivity>(FacultyActivity.class, true,
            true);

    private FacultyActivity facultyActivity = null;

    @Before
    public void setUp() throws Exception {
        facultyActivity = facultyActivityActivityTestRule.getActivity();
    }

    @Test
    public void newFaculty_testLaunch() {
        View view = facultyActivity.findViewById(R.id.rvFacultyList);

        assertNotNull(view);
    }

    @Test
    public void facultyRecyclerView_isVisible() {
        onView(ViewMatchers.withId(R.id.rvFacultyList))
                .check(matches(isDisplayed()));
    }

    @Test
    public void clickAddFacultyButton_opensAddFacultyUi() throws Exception {
        onView(withId(R.id.fabAddFaculty))
                .perform(click());
        onView(withId(R.id.etFacultyName))
                .check(matches(isDisplayed()));
        onView(withId(R.id.etYearOfStudy))
                .check(matches(isDisplayed()));
    }

    @After
    public void newFaculty_tearDown() throws Exception {
        facultyActivity = null;
    }

}
