package com.ferit.tkalcec.digitalschedule.Activities;


import android.app.Activity;
import android.app.Instrumentation;
import android.content.Context;
import android.support.test.espresso.Espresso;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import com.ferit.tkalcec.digitalschedule.R;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;


import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertNotNull;

@RunWith(AndroidJUnit4.class)
public class AddFacultyActivityTest {
    @Rule
    public ActivityTestRule<NewFacultyActivity> newFacultyActivityActivityTestRule = new ActivityTestRule<NewFacultyActivity>(NewFacultyActivity.class);

    private NewFacultyActivity newFacultyActivity = null;
    private String facultyName = "Elektrotehnicki fakultet";
    private String studyName = "Elektrotehnika";
    private String year = "1";

    Instrumentation.ActivityMonitor activityMonitor = getInstrumentation().addMonitor(FacultyActivity.class.getName(),null,false);

    @Before
    public void setUp() throws Exception {
        newFacultyActivity = newFacultyActivityActivityTestRule.getActivity();
    }

    @Before
    public void inputDate() {
        onView(withId(R.id.etFacultyName)).perform(typeText(facultyName));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.etStudyOfFaculty)).perform(typeText(studyName));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.etYearOfStudy)).perform(typeText(year));
        Espresso.closeSoftKeyboard();
    }

    @Test
    public void newFacultyUi_isVisible() {
        onView(withId(R.id.tvFacultyName))
                .check(matches(isDisplayed()));
        onView(withId(R.id.etFacultyName))
                .check(matches(isDisplayed()));
        onView(withId(R.id.tvStudyOfFaculty))
                .check(matches(isDisplayed()));
        onView(withId(R.id.etStudyOfFaculty))
                .check(matches(isDisplayed()));
        onView(withId(R.id.tvYearOfStudy))
                .check(matches(isDisplayed()));
        onView(withId(R.id.etYearOfStudy))
                .check(matches(isDisplayed()));
    }

    @Test
    public void newFaculty_launchFacultyActivityClickOnButton() {
        assertNotNull(newFacultyActivity.findViewById(R.id.btnAddNewFaculty));

        onView(withId(R.id.btnAddNewFaculty)).perform(click());

        Activity facultyActivity = getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 5000);

        assertNotNull(facultyActivity);

        facultyActivity.finish();
    }

    @After
    public void newFaculty_tearDown() throws Exception {
        newFacultyActivity = null;
    }

}
