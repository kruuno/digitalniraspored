# DIGITALNI RASPORED 

## Opis Android UI testova

### LoginActivityTest - sadrži testove za provjeru ispravnosti prikaza UI komponenti prijave te mogućnost prijave
* Test loginActivityTest provjerava mogućnost prijave za uneseno korisničko ime i lozinku te uspješnost prikazivanja UI komponenti tjekom prijave i nakon prijave

### FacultyActivityTest - sadrži testove za provjeru ispravnosti prikaza UI komponenti za prikaz dodanih fakulteta
* Test newFaculty_testLaunch provjerava uspješnost pokretanja Faculty activity-a 
* Test facultyRecyclerView_isVisible provjerava ispravnost prikazivanja RecyclerView liste
* Test clickAddFacultyButton_opensAddFacultyUi provjerava uspješnost otvaranja NewFaculty activity-a nakon odabira gumba za dodjelu novog fakulteta

### AddFacultyActivityTest - sadrži testove za provjeru ispravnosti prikaza UI komponenti za dodavanje novog fakulteta
* Test newFacultyUi_isVisible provjerava uspješnost prikazivanja svih potrebnih UI komponenti za dodavanje novog fakulteta
* Test newFaculty_launchFacultyActivityClickOnButton provjerava uspješnost kreiranja novog fakulteta za dodjeljene podatke

### MainActivityTest - sadrži testove za provjeru ispravnosti prikaza UI komponenti početnog zaslona
* Test clickFacultyListItem_openMainActivity provjerava mogućnost odabira stavke iz liste te mogućnost ulaska u odabrani fakultet
* Test mainActivity_homeFragmentLaunch provjerava ispravnost kreiranja fragmenta za prikaz dodjele kolegija, ispita i predavanja
* Test mainActivity_scheduleFragmentLaunch provjerava ispravnost kreiranja fragmenta za prikaz predavanja
* Test mainActivity_examsFragmentLaunch provjerava ispravnost kreiranja fragmenta za prikaz ispita
