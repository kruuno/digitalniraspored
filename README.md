# DIGITALNI RASPORED

## AUTOR: Dominik Tkalčec

### KRATKI OPIS
Digitalni raspored je android aplikacija namjenjena za dodavanje zakazanih termina predavanja, laboratorijskih, instrukcijskih, audtornih ili konstrukcijskih vježbi te termina pismenih ili usmenih ispita za pojedine kolegije.

### SLIKE
<div align="center">Prijava korisnika.</div><br>
<div align="center"><img src="screenshots/screenshot10.jpg" width="200"></div>
<br></div>
<div align="center">Kreiranje novog fakulteta.</div><br>
<div align="center"><img src="screenshots/screenshot9.jpg" width="200"></div>
<br><br>
<div align="center">Pregled dodanih fakultetam mogućnost izmjene i brisanja</div><br>
<div align="center"><img src="screenshots/screenshot5.jpg" width="200"></div>
<br><br>
<div align="center">Početna prikaz nakon ulaska u željeni fakultet.</div><br>
<div align="center"><img src="screenshots/screenshot8.jpg" width="200"></div>
<br><br>
<div align="center">Klikom na "Kolegiji" vidimo popis svih dodanih kolegija. Klikom na pojedini kolegij vidimo dodatne informacije. Mogućnost izmjene i brisanja.</div><br>
<div align="center"><img src="screenshots/screenshot7.jpg" width="200"></div>
<br><br>
<div align="center">Klikom na "Predavanja" dodajemo predavanja za kreirane kolegije. Klikom na pojedino predavanje vidimo dodatne informacije. Mogućnost izmjene i brisanja.</div><br>
<div align="center"><img src="screenshots/screenshot2.jpg" width="200"></div>
<br><br>
<div align="center">Klikom na "Ispiti" dodajemo ispite za pojedine kolegije. Klikom na pojedini ispit vidimo dodatne informacije. Mogućnost izmjene i brisanja.</div><br>
<div align="center"><img src="screenshots/screenshot1.jpg" width="200"></div>
<br><br>
<div align="center">Klikom na "Predavanja" u donjem izborniku vidimo zakazana predavanja sortirano prema danima, tjednima i mjesecima. Klikom na pojedino predavanje vidimo dodatne informacije (lokacija, dolaznost, vrsta,...).</div><br>
<div align="center"><img src="screenshots/screenshot2.jpg" width="200"></div>
<br><br>
<div align="center">Klikom na "Ispiti" u donjem izborniku vidimo ispite u tijeku te prošle ispite. Klikom na pojedini ispit vidimo dodatne informacija (vrsta, lokacija, vrijeme početka,...).</div><br>
<div align="center"><img src="screenshots/screenshot3.jpg" width="200"></div>

